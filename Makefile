# development test targets

mode  = ljfour
mag   = 4
size  = 10
bglt  = 08
base  = bguq$(size)t$(bglt)
res   = 2400
mf    = $(base).mf
gf    = $(base).$(res)gf
pk    = $(base).$(res)pk
dvi   = $(base).dvi

RUBBISH += $(gf) $(dvi)

$(gf) : $(mf) bguq$(size).mf bguq.mf
	mf '\mode=$(mode); \mag=$(mag); input $(mf)'

$(dvi) : $(gf)
	gftodvi -verbose $(gf)

show : $(dvi)
	xdvi $(dvi)

# maintainece targets

RUBBISH += bguq.aux bguq.log bguq.zip

clean :
	$(RM) $(RUBBISH) 

all :
	sh bguq.sh
	$(RM) bguq.sty
	pdflatex bguq.ins
	pdflatex bguq.dtx

pkg : bguq.zip

bguq.zip :
	mkdir bguq
	cp *.mf *.tfm *.pfb *.fd begriff-bguq.sty bguq/
	for ext in cfg dtx ins map sty ; do \
	  cp bguq.$$ext bguq/ ; \
	done
	cp README.ctan bguq/README.txt
	cp INSTALL bguq/INSTALL.txt
	cp Install.mk bguq/Makefile
	cp bguq.pdf bguq/bguq-doc.pdf
	zip -r bguq bguq
	rm -rf bguq

include Install.mk

